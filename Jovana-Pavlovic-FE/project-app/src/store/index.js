import { createStore } from 'vuex'

export default createStore({
  state: {
    professorList: null,
    studentList: null,
    subjectList: null,
    examList: null,
    examPeriodList: null,
    examRegistrationList: null,
  },
  getters:{
    getProfessorListState: (state) => {
      return state.professorList;
    },
    getStudentListState: (state) => {
      return state.studentList;
    },
    getSubjectListState: (state) => {
      return state.subjectList;
    },
    getExamListState: (state) => {
      return state.examList;
    },
    getExamPeriodListState: (state) => {
      return state.examPeriodList;
    },
    getExamRegistrationListState: (state) => {
      return state.examRegistrationList;
    }
  },
  mutations: {
    storeProfessorPageInfo(state, pageState) {
      state.professorList = {...pageState};
    },
    storeStudentPageInfo(state, pageState) {
      state.studentList = {...pageState};
    },
    storeSubjectPageInfo(state, pageState) {
      state.subjectList = {...pageState};
    },
    storeExamPageInfo(state, pageState) {
      state.examList = {...pageState};
    },
    storeExamPeriodPageInfo(state, pageState) {
      state.examPeriodList = {...pageState};
    },
    storeExamRegistrationInfo(state, pageState) {
      state.examRegistrationList = {...pageState};
    }
  },
  actions: {
    saveProfessorListPageState(context, pageInfo) {
      context.commit('storeProfessorPageInfo', pageInfo)
    }, 
    saveStudentListPageState(context, pageInfo) {
      context.commit('storeStudentPageInfo', pageInfo);
    },
    saveSubjectListPageState(context, pageInfo) {
      context.commit('storeSubjectPageInfo', pageInfo);
    },
    saveExamListPageState(context, pageInfo) {
      context.commit('storeExamPageInfo', pageInfo);
    },
    saveExamPeriodListPageState(context, pageInfo) {
      context.commit('storeExamPeriodPageInfo', pageInfo);
    },
    saveExamRegistrationListPageState(context, pageInfo) {
      context.commit('storeExamRegistrationPageInfo', pageInfo);
    }
  },
  modules: {
  }
})

