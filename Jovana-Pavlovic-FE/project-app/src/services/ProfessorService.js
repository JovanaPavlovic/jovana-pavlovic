import { projectClient } from '../main.js'
  
export default {
  getAllProfessors: () => {
    return projectClient.get('professor')
  },
  getProfessorsByPage: ({ page, size }) => {
    return projectClient.get(`professor/page?page=${page}&size=${size}`)
  },
  getProfessorById: (professorId) => {
    return projectClient.get(`professor/find/${professorId}`)
  },
  insertProfessor: (professor) => {
    return projectClient.post('professor/save', professor)
  },
  editProfessor: (professor) => {
    return projectClient.post('professor/update', professor)
  },
  deleteProfessor(professorId) {
    return projectClient.delete(`professor/delete/${professorId}`)
  }
}