import { projectClient } from '../main.js'
  
export default {
  getAllCities: () => {
    return projectClient.get('city')
  }
}