import { projectClient } from '../main.js'
  
export default {
  getAllExamPeriods: () => {
    return projectClient.get('examPeriod')
  },
  getExamPeriodsByPage: ({ page, size }) => {
    return projectClient.get(`examPeriod/page?page=${page}&size=${size}`)
  },
  getExamPeriodById: (examPeriodId) => {
    return projectClient.get(`examPeriod/find/${examPeriodId}`)
  },
  insertExamPeriod: (examPeriod) => {
    return projectClient.post('examPeriod/save', examPeriod)
  },
  editExamPeriod: (examPeriod) => {
    return projectClient.post('examPeriod/update', examPeriod)
  },
  deleteExamPeriod(examPeriodId) {
    return projectClient.delete(`examPeriod/delete/${examPeriodId}`)
  }
}