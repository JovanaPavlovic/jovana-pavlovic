import { projectClient } from '../main.js'
  
export default {
  getAllStudents: () => {
    return projectClient.get('student')
  },
  getStudentsByPage: ({ page, size }) => {
    return projectClient.get(`student/page?page=${page}&size=${size}`)
  },
  getStudentById: (studentId) => {
    return projectClient.get(`student/find/${studentId}`)
  },
  insertStudent: (student) => {
    return projectClient.post('student/save', student)
  },
  editStudent: (student) => {
    return projectClient.post('student/update', student)
  },
  deleteStudent(studentId) {
    return projectClient.delete(`student/delete/${studentId}`)
  }
}