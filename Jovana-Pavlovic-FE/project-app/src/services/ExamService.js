import { projectClient } from '../main.js'
  
export default {
  getAllExams: () => {
    return projectClient.get('exam')
  },
  getExamsByPage: ({ page, size }) => {
    return projectClient.get(`exam/page?page=${page}&size=${size}`)
  },
  getExamById: (examId) => {
    return projectClient.get(`exam/find/${examId}`)
  },
  insertExam: (exam) => {
    return projectClient.post('exam/save', exam)
  },
  editExam: (exam) => {
    return projectClient.post('exam/update', exam)
  },
  deleteExam(examId) {
    return projectClient.delete(`exam/delete/${examId}`)
  }
}