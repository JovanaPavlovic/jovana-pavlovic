import { projectClient } from '../main.js'
  
export default {
  getAllExamRegistrations: () => {
    return projectClient.get('examRegistration')
  },
  getExamRegistrationsByPage: ({ page, size }) => {
    return projectClient.get(`examRegistration/page?page=${page}&size=${size}`)
  },
  getExamRegistrationById: (examRegistrationId) => {
    return projectClient.get(`examRegistration/find/${examRegistrationId}`)
  },
  insertExamRegistration: (examRegistration) => {
    return projectClient.post('examRegistration/save', examRegistration)
  },
  editExamRegistration: (examRegistration) => {
    return projectClient.post('examRegistration/update', examRegistration)
  },
  deleteExamRegistration(examRegistrationId) {
    return projectClient.delete(`examRegistration/delete/${examRegistrationId}`)
  }
}