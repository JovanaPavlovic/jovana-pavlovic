import { projectClient } from '../main.js'
  
export default {
  getAllTitles: () => {
    return projectClient.get('title')
  }
}