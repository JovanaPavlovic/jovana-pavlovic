import { projectClient } from '../main.js'
  
export default {
  getAllSubjects: () => {
    return projectClient.get('subject')
  },
  getSubjectsByPage: ({ page, size }) => {
    return projectClient.get(`subject/page?page=${page}&size=${size}`)
  },
  getSubjectById: (subjectId) => {
    return projectClient.get(`subject/find/${subjectId}`)
  },
  insertSubject: (subject) => {
    return projectClient.post('subject/save', subject)
  },
  editSubject: (subject) => {
    return projectClient.post('subject/update', subject)
  },
  deleteSubject(subjectId) {
    return projectClient.delete(`subject/delete/${subjectId}`)
  }
}