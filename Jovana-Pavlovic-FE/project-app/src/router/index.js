import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import ProfessorList from '../views/professor/ProfessorList.vue'
import ProfessorForm from '../views/professor/ProfessorForm.vue'
import ProfessorDetails from '../views/professor/ProfessorDetails.vue'
import StudentList from '../views/student/StudentList.vue'
import StudentForm from '../views/student/StudentForm.vue'
import StudentDetails from '../views/student/StudentDetails.vue'
import SubjectList from '../views/subject/SubjectList.vue'
import SubjectForm from '../views/subject/SubjectForm.vue'
import SubjectDetails from '../views/subject/SubjectDetails.vue'
import ExamList from '../views/exam/ExamList.vue'
import ExamForm from '../views/exam/ExamForm.vue'
import ExamDetails from '../views/exam/ExamDetails.vue'
import ExamPeriodList from '../views/examPeriod/ExamPeriodList.vue'
import ExamPeriodForm from '../views/examPeriod/ExamPeriodForm.vue'
import ExamPeriodDetails from '../views/examPeriod/ExamPeriodDetails.vue'
import ExamRegistrationList from '../views/examRegistration/ExamRegistrationList.vue'
import ExamRegistrationForm from '../views/examRegistration/ExamRegistrationForm.vue'
import ExamRegistrationDetails from '../views/examRegistration/ExamRegistrationDetails.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/professors',
    name: 'ProfessorList',
    component: ProfessorList
  },
  {
    path: '/professor-form/:professorId',
    props: true,
    name: 'EditProfessor',
    component: ProfessorForm
  },
  {
    path: '/professor-form',   
    props: { actionType: 'new'}, 
    name: 'NewProfessor',
    component: ProfessorForm
  },
  {
    path: '/professor-details/:professorId',
    props: true,
    name: 'ProfessorDetails',
    component: ProfessorDetails
  },
  {
    path: '/students',
    name: 'StudentList',
    component: StudentList
  },
  {
    path: '/student-form/:studentId',
    props: true,
    name: 'EditStudent',
    component: StudentForm
  },
  {
    path: '/student-form',   
    props: { actionType: 'new'}, 
    name: 'NewStudent',
    component: StudentForm
  },
  {
    path: '/student-details/:studentId',
    props: true,
    name: 'StudentDetails',
    component: StudentDetails
  },
  {
    path: '/subjects',
    name: 'SubjectList',
    component: SubjectList
  },
  {
    path: '/subject-form/:subjectId',
    props: true,
    name: 'EditSubject',
    component: SubjectForm
  },
  {
    path: '/subject-form',   
    props: { actionType: 'new'}, 
    name: 'NewSubject',
    component: SubjectForm
  },
  {
    path: '/subject-details/:subjectId',
    props: true,
    name: 'SubjectDetails',
    component: SubjectDetails
  },
  {
    path: '/exams',
    name: 'ExamList',
    component: ExamList
  },
  {
    path: '/exam-form/:examId',
    props: true,
    name: 'EditExam',
    component: ExamForm
  },
  {
    path: '/exam-form',   
    props: { actionType: 'new'}, 
    name: 'NewExam',
    component: ExamForm
  },
  {
    path: '/exam-details/:examId',
    props: true,
    name: 'ExamDetails',
    component: ExamDetails
  },
  {
    path: '/examPeriods',
    name: 'ExamPeriodList',
    component: ExamPeriodList
  },
  {
    path: '/exam-period-form/:examPeriodId',   
    props: true, 
    name: 'EditExamPeriod',
    component: ExamPeriodForm
  },
  {
    path: '/exam-period-form',   
    props: { actionType: 'new'}, 
    name: 'NewExamPeriod',
    component: ExamPeriodForm
  },
  {
    path: '/exam-period-details/:examPeriodId',
    props: true,
    name: 'ExamPeriodDetails',
    component: ExamPeriodDetails
  },
  {
    path: '/examRegistrations',
    name: 'ExamRegistrationList',
    component: ExamRegistrationList
  },
  {
    path: '/exam-registration-form/:examRegistrationId',   
    props: true, 
    name: 'EditExamRegistration',
    component: ExamRegistrationForm
  },
  {
    path: '/exam-registration-form',   
    props: { actionType: 'new'}, 
    name: 'NewExamRegistration',
    component: ExamRegistrationForm
  },
  {
    path: '/exam-registration-details/:examRegistrationId',
    props: true,
    name: 'ExamRegistrationDetails',
    component: ExamRegistrationDetails
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
