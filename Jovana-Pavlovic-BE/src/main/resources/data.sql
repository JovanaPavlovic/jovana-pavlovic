-- Insert for City
-- *************************************************

INSERT INTO City (postalcode, name) VALUES (11080, 'Novi Beograd');
INSERT INTO City (postalcode, name) VALUES (11000, 'Beograd');
INSERT INTO City (postalcode, name) VALUES (75420, 'Bratunac');
INSERT INTO City (postalcode, name) VALUES (34300, 'Arandjelovac');
INSERT INTO City (postalcode, name) VALUES (31250, 'Bajina Basta');
INSERT INTO City (postalcode, name) VALUES (37000, 'Krusevac');
INSERT INTO City (postalcode, name) VALUES (15320, 'Ljubovija');
INSERT INTO City (postalcode, name) VALUES (15300, 'Loznica');
INSERT INTO City (postalcode, name) VALUES (15318, 'Mali Zvornik');
INSERT INTO City (postalcode, name) VALUES (11400, 'Mladenovac');
INSERT INTO City (postalcode, name) VALUES (19300, 'Negotin');
INSERT INTO City (postalcode, name) VALUES (21000, 'Novi Sad');
INSERT INTO City (postalcode, name) VALUES (26000, 'Pancevo');


-- Insert for Student
-- *************************************************

INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0065', '2020', 'Jovana', 'Pavlovic', 'pavlovicjovana71@gmail.com', 'Sejkina 66a', 11000, 1);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0004', '2016', 'Darija', 'Gavric', 'darijag5@gmail.com', 'Drinska bb', 34300, 4);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0057', '2016', 'Milovan', 'Ostojic', 'milo@gmail.com', 'Slavonska ulica', 11000, 4);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0155', '2018', 'Nemanja', 'Simic', 'simic97@gmail.com', 'Cerova', 75420, 3);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0110', '2017', 'Goran', 'Beatovic', 'beat@gmail.com', 'Dusana Kvedera 13', 26000, 4);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0043', '2019', 'Branka', 'Ribac', 'brankaribac@gmail.com', 'Dostojevskog', 75420, 2);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0199', '2018', 'Milica', 'Vidojevic', 'milica77@gmail.com', 'Zmajevacka', 15318, 3);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0139', '2020', 'Milomir', 'Simic', 'simicm@gmail.com', 'Valjevska', 31250, 1);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0256', '2020', 'Milos', 'Ilic', 'ilicmilos@gmail.com', 'Ulica Svetog Save', 37000, 1);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0245', '2018', 'Jana', 'Prokic', 'prokiceva3@gmail.com', 'Vase Stajica', 75420, 3);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0314', '2019', 'Ivona', 'Nedjic', 'ivona177@gmail.com', 'Vojna Posta', 26000, 2);
INSERT INTO Student (id, indexnumber, indexyear, firstname, lastname, email, address, postalcode, current_year_of_study) 
	VALUES (student_id_seq.NEXTVAL, '0203', '2019', 'Teodora', 'Gavric', 'teagavric@gmail.com', 'Avalska', 11080, 2);


-- Insert for Subject
-- *************************************************

INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Analytic geometry', 'Concept and properties of vectors in plane and space.', '6', 1, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Linear algebra', 'Semi-group, group. Ring, field; polynomials and matrices.', '9', 1, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Algorithm theory', 'Turing machines, recursive functions and other computational systems', '8', 1, 'Summer');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Database systems', 'Database management systems; functions; architecture; data independence.', '7', 2, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Object oriented programming', 'Object-oriented paradigm. Objects, classes, inheritance.', '5', 2, 'Summer');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Programming paradigms', 'An overview of the development of program paradigms.', '5', 3, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Software development', 'Software development problem. An overview of modern development methodologies. UML', '7', 3, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Differential equations', 'First order differential equations, differential higher order equations', '7', 3, 'Summer');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Programming language design', 'Definitions and characteristics of programming languages.', '5', 3, 'Summer');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Operating systems', 'Introduction to operating systems: basic concepts; historical overview.', '8', 4, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Mathematic logic in computing', 'Boolean algebra. Basic properties and more important examples.', '6', 4, 'Winter');
INSERT INTO Subject (id, name, description, number_of_ESPB, year_of_study, semester) 
	VALUES (subject_id_seq.NEXTVAL, 'Computer networks', 'History and classification of computer networks.', '6', 4, 'Summer');


-- Insert for Title
-- *************************************************

INSERT INTO Title (id, name) VALUES (title_id_seq.NEXTVAL, 'Emeritus');
INSERT INTO Title (id, name) VALUES (title_id_seq.NEXTVAL, 'Professor');
INSERT INTO Title (id, name) VALUES (title_id_seq.NEXTVAL, 'Associate Professor');
INSERT INTO Title (id, name) VALUES (title_id_seq.NEXTVAL, 'Assistant');


-- Insert for Professor
-- *************************************************

INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Mico', 'Peric', 'mp@gmail.com', 'Trg Republike 55b', 11000, '+381644321402', '24-nov-2022', 2);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Dajana', 'Stankovic', 'dajanas77@gmail.com', 'Kralja Petra 18', 11080, '+381653344911', '13-feb-2023', 2);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Igor', 'Mitic', 'miticigor@gmail.com', 'Nemanjina 5', 11400, '+3816512345677', '25-jan-2023', 4);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Momcilo', 'Popovic', 'momcilo87@gmail.com', 'Ulica Svetog Save', 11000, '+381643709843', '24-jun-2023', 3);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Jelica', 'Pavlovic', 'pavlovicjelica@gmail.com', 'Bezanijsk', 19300, '+381644567455', '15-jun-2023', 3);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Sinisa', 'Vasic', 'vasic@gmail.com', 'Bulevar Mira', 75420, '+381657745231', '08-jan-2022', 1);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Njegos', 'Maksimovic', 'maksimovicnj@gmail.com', 'Bosanska', 37000, '+381608978679', '19-sep-2025', 2);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Milos', 'Vasic', 'misovasic5@gmail.com', 'Bulevar Avnoja', 11080, '+381601234356', '01-nov-2025', 3);
INSERT INTO Professor (id, firstname, lastname, email, address, postalcode, phone, reelection_date, title) 
	VALUES (professor_id_seq.NEXTVAL, 'Igor', 'Macanovic', 'igor23@gmail.com', 'Admirala Geprata', 15320, '+381654523988', '27-dec-2022', 4);


-- Insert for Exam_Period
-- *************************************************

INSERT INTO Exam_Period (id, name, start_of_exam_period, end_of_exam_period, active) 
	VALUES (exam_period_id_seq.NEXTVAL, 'January 2021', '15-jan-2021', '30-jan-2021', 0);
INSERT INTO Exam_Period (id, name, start_of_exam_period, end_of_exam_period, active) 
	VALUES (exam_period_id_seq.NEXTVAL, 'February 2021', '01-feb-2021', '15-feb-2021', 0);
INSERT INTO Exam_Period (id, name, start_of_exam_period, end_of_exam_period, active) 
	VALUES (exam_period_id_seq.NEXTVAL, 'Jun 2021', '15-jun-2021', '30-jun-2021', 0);
INSERT INTO Exam_Period (id, name, start_of_exam_period, end_of_exam_period, active) 
	VALUES (exam_period_id_seq.NEXTVAL, 'July 2021', '01-jul-2021', '15-jul-2021', 1);
INSERT INTO Exam_Period (id, name, start_of_exam_period, end_of_exam_period, active) 
	VALUES (exam_period_id_seq.NEXTVAL, 'September 2021', '01-sep-2021', '15-sep-2021', 0);
INSERT INTO Exam_Period (id, name, start_of_exam_period, end_of_exam_period, active) 
	VALUES (exam_period_id_seq.NEXTVAL, 'October 2021', '16-sep-2021', '01-oct-2021', 1);


-- Insert for Exam
-- *************************************************

INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 2, 4, '27-jan-2021', 1);
INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 7, 8, '20-jan-2021', 1);
INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 6, 8, '03-feb-2021', 2);
INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 12, 2, '05-jul-2021', 4);
INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 5, 9, '10-jul-2021', 4);
INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 11, 5, '13-sep-2021', 5);
INSERT INTO Exam (id, subject_id, professor_id, exam_date, exam_period_id) 
	VALUES (exam_id_seq.NEXTVAL, 9, 1, '23-sep-2021', 6);


-- Insert for Exam_Registration
-- *************************************************

INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 1, 1, '23-jan-2021');
INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 5, 2, '14-jan-2021');
INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 4, 3, '01-feb-2021');
INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 2, 4, '01-jul-2021');
INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 11, 5, '05-jul-2021');
INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 3, 6, '09-sep-2021');
INSERT INTO Exam_Registration (id, student_id, exam_id, registration_date) 
	VALUES (exam_registration_id_seq.NEXTVAL, 10, 7, '20-sep-2021');