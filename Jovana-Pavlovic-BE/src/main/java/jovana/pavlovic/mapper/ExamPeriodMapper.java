package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.ExamPeriodDto;
import jovana.pavlovic.enity.ExamPeriodEntity;

@Mapper
public interface ExamPeriodMapper {
	ExamPeriodDto toExamPeriodDto(ExamPeriodEntity examPeriodEntity);
	ExamPeriodEntity toExamPeriodEntity(ExamPeriodDto examPeriodDto);
}
