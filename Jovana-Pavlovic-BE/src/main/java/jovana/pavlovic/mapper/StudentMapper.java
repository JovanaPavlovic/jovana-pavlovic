package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.StudentDto;
import jovana.pavlovic.enity.StudentEntity;

@Mapper
public interface StudentMapper {
	StudentDto toStudentDto(StudentEntity studentEntity);
	StudentEntity toStudentEntity(StudentDto studentDto);

}
