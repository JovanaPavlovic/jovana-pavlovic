package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.ExamDto;
import jovana.pavlovic.enity.ExamEntity;

@Mapper
public interface ExamMapper {
	ExamDto toExamDto(ExamEntity examEntity);
	ExamEntity toExamEntity(ExamDto examDto);

}
