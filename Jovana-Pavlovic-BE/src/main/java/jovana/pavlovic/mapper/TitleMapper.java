package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.TitleDto;
import jovana.pavlovic.enity.TitleEntity;

@Mapper
public interface TitleMapper {
	TitleDto toTitleDto(TitleEntity titleEntity);

}
