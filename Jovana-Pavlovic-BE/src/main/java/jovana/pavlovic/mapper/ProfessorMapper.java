package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.ProfessorDto;
import jovana.pavlovic.enity.ProfessorEntity;

@Mapper
public interface ProfessorMapper {
	ProfessorDto toProfessorDto(ProfessorEntity professorEntity);
	ProfessorEntity toProfessorEntity(ProfessorDto professorDto);

}
