package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.CityDto;
import jovana.pavlovic.enity.CityEntity;

@Mapper
public interface CityMapper {
	CityDto toCityDto(CityEntity cityEntity);

}
