package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.SubjectDto;
import jovana.pavlovic.enity.SubjectEntity;

@Mapper
public interface SubjectMapper {
	SubjectDto toSubjectDto(SubjectEntity subjectEntity);
	SubjectEntity toSubjectEntity(SubjectDto subjectDto);

}
