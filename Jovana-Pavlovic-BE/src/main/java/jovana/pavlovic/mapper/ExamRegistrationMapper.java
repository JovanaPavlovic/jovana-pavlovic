package jovana.pavlovic.mapper;

import org.mapstruct.Mapper;

import jovana.pavlovic.dto.ExamRegistrationDto;
import jovana.pavlovic.enity.ExamRegistrationEntity;

@Mapper
public interface ExamRegistrationMapper {
	ExamRegistrationDto toExamRegistrationDto(ExamRegistrationEntity examRegistrationEntity);
	ExamRegistrationEntity toExamRegistrationEntity(ExamRegistrationDto examRegistrationDto);

}
