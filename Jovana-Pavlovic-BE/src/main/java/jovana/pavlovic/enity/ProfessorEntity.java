package jovana.pavlovic.enity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "professor")
public class ProfessorEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ProfessorGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ProfessorGen", sequenceName = "professor_id_seq", allocationSize = 1, initialValue = 1)	
    private Long id;
	
	@Column(nullable=false, length=30)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String firstname;
	
	@Column(nullable=false, length=30)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String lastname;
	
	@Column(unique=true, length=30)
	@Email(message = "Invalid email.")
	private String email;
	
	@Column(length=50)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String address;
	
	@ManyToOne
	@JoinColumn(name = "postalcode")
	//@Range(min = 1, max = 5, message = "Postal code requires max 5 digits.")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CityEntity city;
	
	@Column(length=15)
	@Size(min = 6, message = "It's necessary to enter minimum 6 characters.")
	private String phone;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date reelectionDate;
	
	@ManyToOne
	@JoinColumn(name = "title", nullable=false)
	//@Range(min = 1, max = 7, message = "Title id requires max 7 digits.")
	private TitleEntity title;
	
	public ProfessorEntity() {
		
	}

	public ProfessorEntity(Long id, String firstname, String lastname, String email, String address, CityEntity city,
			String phone, Date reelectionDate, TitleEntity title) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityEntity getCity() {
		return city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleEntity getTitle() {
		return title;
	}

	public void setTitle(TitleEntity title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "ProfessorEntity [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", address=" + address + ", city=" + city + ", phone=" + phone + ", reelectionDate=" + reelectionDate
				+ ", title=" + title + "]";
	}

} 