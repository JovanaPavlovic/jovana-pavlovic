package jovana.pavlovic.enity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "title")
public class TitleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "TitleGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "TitleGen", sequenceName = "title_id_seq", allocationSize = 1, initialValue = 1)	
	private Long id;
	
	@Column(nullable=false, length=30)
	private String name;
	
	public TitleEntity() {
		
	}

	public TitleEntity(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TitleEntity [id=" + id + ", name=" + name + "]";
	}

}