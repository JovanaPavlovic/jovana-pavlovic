package jovana.pavlovic.enity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class CityEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="postalcode")
	private Long postalCode;
	
	@Column(nullable=false, length=200)
	private String name;
	
	public CityEntity() {
		
	}

	public CityEntity(Long postalCode, String name) {
		super();
		this.postalCode = postalCode;
		this.name = name;
	}

	public Long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityEntity [postalCode=" + postalCode + ", name=" + name + "]";
	}
	
}
