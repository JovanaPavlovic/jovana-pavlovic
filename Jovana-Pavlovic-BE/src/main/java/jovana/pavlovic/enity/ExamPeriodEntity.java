package jovana.pavlovic.enity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "exam_period")
public class ExamPeriodEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "ExamPeriodGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ExamPeriodGen", sequenceName = "exam_period_id_seq", allocationSize = 1, initialValue = 1)	
	private Long id;
	
	@Column(nullable=false, length=30)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String name;
	
	@Column(nullable=false)
	private LocalDate startOfExamPeriod;
	
	@Column(nullable=false)
	private LocalDate endOfExamPeriod;

	private boolean active;

	@OneToMany(mappedBy = "examPeriod", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ExamEntity> exams;
	
	public ExamPeriodEntity() {
		this.exams = new ArrayList<>();
	}

	public ExamPeriodEntity(Long id, String name, LocalDate startOfExamPeriod, LocalDate endOfExamPeriod, boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.startOfExamPeriod = startOfExamPeriod;
		this.endOfExamPeriod = endOfExamPeriod;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getStartOfExamPeriod() {
		return startOfExamPeriod;
	}

	public void setStartOfExamPeriod(LocalDate startOfExamPeriod) {
		this.startOfExamPeriod = startOfExamPeriod;
	}

	public LocalDate getEndOfExamPeriod() {
		return endOfExamPeriod;
	}

	public void setEndOfExamPeriod(LocalDate endOfExamPeriod) {
		this.endOfExamPeriod = endOfExamPeriod;
	}

	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	
	public List<ExamEntity> getExams() {
		return new ArrayList<ExamEntity>(exams);
	}
	
	@Override
	public String toString() {
		return "ExamPeriodEntity [id=" + id + ", name=" + name + ", startOfExamPeriod=" + startOfExamPeriod + ", endOfExamPeriod=" + endOfExamPeriod + ", active=" + active
				+ ", exams=" + exams + "]";
	}
	
	public void addExam(ExamEntity exam) {
		if(exams.contains(exam)) return;
		exams.add(exam);
		exam.setExamPeriod(this);
	} 
	
	public void removeExam(ExamEntity exam) {
	    if (!exams.contains(exam)) return ;
	    exams.remove(exam);
	    exam.setExamPeriod(null);
	  }

}
