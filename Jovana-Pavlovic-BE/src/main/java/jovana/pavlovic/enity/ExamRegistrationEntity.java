package jovana.pavlovic.enity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "exam_registration")
public class ExamRegistrationEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "ExamRegGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ExamRegGen", sequenceName = "exam_registration_id_seq", allocationSize = 1, initialValue = 1)	
	private Long id;

	@ManyToOne		
	@JoinColumn(name = "student_id", nullable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private StudentEntity student;
	
	@ManyToOne	
	@JoinColumn(name = "exam_id", nullable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ExamEntity exam;
	
	@Column(nullable=false)
	private LocalDate registrationDate;
	
	public ExamRegistrationEntity() {
		
	}

	public ExamRegistrationEntity(Long id, StudentEntity student, ExamEntity exam, LocalDate registrationDate) {
		super();
		this.id = id;
		this.student = student;
		this.exam = exam;
		this.registrationDate = registrationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StudentEntity getStudent() {
		return student;
	}

	public void setStudent(StudentEntity student) {
		this.student = student;
	}

	public ExamEntity getExam() {
		return exam;
	}

	public void setExam(ExamEntity exam) {
		this.exam = exam;
	}

	public LocalDate getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDate registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public String toString() {
		return "ExamRegistrationEntity [id=" + id + ", student=" + student + ", exam=" + exam + ", registrationDate=" + registrationDate + "]";
	}
	
}
