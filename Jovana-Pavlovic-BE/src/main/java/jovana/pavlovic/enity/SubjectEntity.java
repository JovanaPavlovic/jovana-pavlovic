package jovana.pavlovic.enity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "subject")
public class SubjectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "SubjectGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "SubjectGen", sequenceName = "subject_id_seq", allocationSize = 1, initialValue = 1)	
	private Long id;
	
	@Column(nullable=false, length=30)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String name;
	
	@Column(length=200)
	private String description;
	
	@Column(name="number_of_ESPB", nullable=false)
	private Long numberOfESPB;

	private Long yearOfStudy;
	
	@Column(length=10)
	@Pattern(regexp = "Summer|Winter", message = "Allowed values are 'Summer' and 'Winter'")
	private String semester;
	
	public SubjectEntity() {
		
	}

	public SubjectEntity(Long id, String name, String description, Long numberOfESPB, Long yearOfStudy,
			String semester) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.numberOfESPB = numberOfESPB;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getNumberOfESPB() {
		return numberOfESPB;
	}

	public void setNumberOfESPB(Long numberOfESPB) {
		this.numberOfESPB = numberOfESPB;
	}

	public Long getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(Long yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	@Override
	public String toString() {
		return "SubjectEntity [id=" + id + ", name=" + name + ", description=" + description + ", numberOfESPB="
				+ numberOfESPB + ", yearOfStudy=" + yearOfStudy + ", semester=" + semester + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectEntity other = (SubjectEntity) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
} 
