package jovana.pavlovic.enity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "student", uniqueConstraints = {@UniqueConstraint(columnNames = {"indexyear" , "indexnumber"})})
public class StudentEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "StudentGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "StudentGen", sequenceName = "student_id_seq", allocationSize = 1, initialValue = 1)	
	private Long id;
	
	@Column(name="indexnumber", nullable=false, length=4)
	@Size(min = 4, max = 4, message = "It's necessary to enter exact 4 characters.")
	private String indexNumber;
	
	@Column(name="indexyear", nullable=false, length=4)
	private String indexYear;
	
	@Column(nullable=false, length=30)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String firstname;
	
	@Column(nullable=false, length=30)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String lastname;
	
	@Column(unique=true, length=30)
	@Email(message = "Invalid email.")
	private String email;
	
	@Column(length=50)
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String address;
	
	@ManyToOne
	@JoinColumn(name = "postalcode")
	@OnDelete(action = OnDeleteAction.CASCADE)
	//@Range(min = 1, max = 5, message = "Postal code requires max 5 digits.")
	private CityEntity city;
	
	@Column(nullable=false)
	//@Range(min = 1, max = 7, message = "Current year od study requires max 7 digits.")
	private Long currentYearOfStudy;
	
	
	public StudentEntity() {
		
	}

	public StudentEntity(Long id, String indexNumber, String indexYear, String firstname, String lastname, String email, String address, CityEntity city,
			Long currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.indexYear = indexYear;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.city = city;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getIndexNumber() {
		return indexNumber;
	}


	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getIndexYear() {
		return indexYear;
	}


	public void setIndexYear(String indexYear) {
		this.indexYear = indexYear;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}

	public CityEntity getCity() {
		return city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}


	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}


	@Override
	public String toString() {
		return "StudentEntity [id=" + id + ", indexNumber=" + indexNumber + ", indexYear=" + indexYear + ", firstname="
				+ firstname + ", lastname=" + lastname + ", email=" + email + ", address=" + address + ", city="
				+ city + ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	
} 

