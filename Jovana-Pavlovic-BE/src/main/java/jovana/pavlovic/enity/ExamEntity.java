package jovana.pavlovic.enity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "exam")
public class ExamEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ExamGen", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ExamGen", sequenceName = "exam_id_seq", allocationSize = 1, initialValue = 1)	
	private Long id;
	
	@ManyToOne				
	@JoinColumn(name = "subject_id", nullable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SubjectEntity subject;
	
	@ManyToOne	
	@JoinColumn(name = "professor_id", nullable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ProfessorEntity professor;
	
	@Column(nullable=false)
	private LocalDate examDate; 
	
	@ManyToOne
	private ExamPeriodEntity examPeriod;
	
	public ExamEntity() {
		
	}

	public ExamEntity(Long id, SubjectEntity subject, ProfessorEntity professor, LocalDate examDate,
			ExamPeriodEntity examPeriod) {
		super();
		this.id = id;
		this.subject = subject;
		this.professor = professor;
		this.examDate = examDate;
		this.examPeriod = examPeriod;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	public ProfessorEntity getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorEntity professor) {
		this.professor = professor;
	}

	public LocalDate getExamDate() {
		return examDate;
	}

	public void setExamDate(LocalDate examDate) {
		this.examDate = examDate;
	}

	public ExamPeriodEntity getExamPeriod() {
		return examPeriod;
	}

	public void setExamPeriod(ExamPeriodEntity examPeriod) {
		this.examPeriod = examPeriod;
	}

	@Override
	public String toString() {
		return "ExamEntity [id=" + id + ", subject=" + subject + ", professor=" + professor + ", examDate=" + examDate
				+ ", examPeriod=" + examPeriod + "]";
	}
	
}
