package jovana.pavlovic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JovanaPavlovicBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JovanaPavlovicBeApplication.class, args);
	}

}
