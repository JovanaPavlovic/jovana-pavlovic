package jovana.pavlovic.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProfessorDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotEmpty(message = "Firstname can't be empty.")
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String firstname;
	
	@NotEmpty(message = "Lastname can't be empty.")
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String lastname;
	
	@Email(message = "Invalid email.")
	private String email;
	
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String address;

	private CityDto city;
	
	@Size(min = 6, message = "It's necessary to enter minimum 6 characters.")
	private String phone;

	@NotNull(message = "Reelection date can't be null.")
	private Date reelectionDate;
	
	@NotNull(message = "Title can't be null.")
	//@Range(min = 1, max = 7, message = "Title id requires max 7 digits.")
	private TitleDto title;
	
	public ProfessorDto() {
		
	}

	public ProfessorDto(Long id, String firstname, String lastname, String email, String address, CityDto city, String phone,
			Date reelectionDate, TitleDto title) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleDto getTitle() {
		return title;
	}

	public void setTitle(TitleDto title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", address="
				+ address + ", city=" + city + ", phone=" + phone + ", reelectionDate=" + reelectionDate + ", title="
				+ title + "]";
	}

}
