package jovana.pavlovic.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotEmpty(message = "Index number can't be empty.")
	private String indexNumber;
	
	@NotEmpty(message = "Index year can't be empty.")
	@Size(min = 4, max = 4, message = "It's necessary to enter exact 4 characters.")
	private String indexYear;
	
	@NotEmpty(message = "Firstname can't be empty.")
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String firstname;
	
	@NotEmpty(message = "Lastname can't be empty.")
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String lastname;
	
	@Email(message = "Invalid email.")
	private String email;
	
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String address;
	
	private CityDto city;
	
	@NotNull(message = "Current year of study can't be null.")
	private Long currentYearOfStudy;
	
	public StudentDto() {
		
	}

	public StudentDto(Long id, String indexNumber, String indexYear, String firstname, String lastname, String email,
			String address, CityDto city, Long currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.indexYear = indexYear;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address = address;
		this.city = city;
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getIndexYear() {
		return indexYear;
	}

	public void setIndexYear(String indexYear) {
		this.indexYear = indexYear;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", indexYear=" + indexYear + ", firstname="
				+ firstname + ", lastname=" + lastname + ", email=" + email + ", address=" + address + ", city=" + city
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	
}
