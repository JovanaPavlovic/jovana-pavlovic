package jovana.pavlovic.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ExamPeriodDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotEmpty(message = "Name can't be empty.")
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String name;
	
	@NotNull(message = "Start of exam period can't be null.")
	private LocalDate startOfExamPeriod;
	
	@NotNull(message = "End of exam period can't be null.")
	private LocalDate endOfExamPeriod;
	
	private boolean active;
	private List<ExamDto> exams;
	
	public ExamPeriodDto() {
		this.exams = new ArrayList<>();
	}

	public ExamPeriodDto(Long id, String name, LocalDate startOfExamPeriod, LocalDate endOfExamPeriod, boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.startOfExamPeriod = startOfExamPeriod;
		this.endOfExamPeriod = endOfExamPeriod;
		this.active = active;
		this.exams = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getStartOfExamPeriod() {
		return startOfExamPeriod;
	}

	public void setStartOfExamPeriod(LocalDate startOfExamPeriod) {
		this.startOfExamPeriod = startOfExamPeriod;
	}

	public LocalDate getEndOfExamPeriod() {
		return endOfExamPeriod;
	}

	public void setEndOfExamPeriod(LocalDate endOfExamPeriod) {
		this.endOfExamPeriod = endOfExamPeriod;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<ExamDto> getExams() {
		return exams;
	}

	public void setExams(List<ExamDto> exams) {
		this.exams = exams;
	}

	@Override
	public String toString() {
		return "ExamPeriodDto [name=" + name + ", startOfExamPeriod=" + startOfExamPeriod + ", endOfExamPeriod="
				+ endOfExamPeriod + ", active=" + active + ", exams=" + exams + "]";
	}

}
