package jovana.pavlovic.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SubjectDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotEmpty(message = "Name can't be empty.")
	@Size(min = 3, message = "It's necessary to enter minimum 3 characters.")
	private String name;
	
	private String description;
	
	@NotNull(message = "Number of ESPB can't be null.")
	private Long numberOfESPB;
	
	private Long yearOfStudy;
	
	@Pattern(regexp = "Summer|Winter", message="Allowed values are 'Summer' and 'Winter'")
	private String semester;
	
	public SubjectDto() {
		
	}

	public SubjectDto(Long id, String name, String description, Long numberOfESPB, Long yearOfStudy, String semester) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.numberOfESPB = numberOfESPB;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getNumberOfESPB() {
		return numberOfESPB;
	}

	public void setNumberOfESPB(Long numberOfESPB) {
		this.numberOfESPB = numberOfESPB;
	}

	public Long getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(Long yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	@Override
	public String toString() {
		return "SubjectDto [id=" + id + ", name=" + name + ", description=" + description + ", numberOfESPB=" + numberOfESPB + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectDto other = (SubjectDto) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
