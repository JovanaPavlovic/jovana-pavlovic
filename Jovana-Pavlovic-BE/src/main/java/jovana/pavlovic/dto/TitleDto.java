package jovana.pavlovic.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

public class TitleDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotEmpty(message = "Name can't be empty.")
	private String name;
	
	public TitleDto() {
		
	}

	public TitleDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TitleDto [id=" + id + ", name=" + name + "]";
	}

}
