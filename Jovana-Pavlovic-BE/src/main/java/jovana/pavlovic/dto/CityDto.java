package jovana.pavlovic.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CityDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "Postalcode can't be null.")
	private Long postalCode;
	
	@NotEmpty(message = "Name can't be empty.")
	private String name;
	
	public CityDto() {
		
	}

	public CityDto(Long postalCode, String name) {
		super();
		this.postalCode = postalCode;
		this.name = name;
	}

	public Long getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(Long postalCode) {
		this.postalCode = postalCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CityDto [postalCode=" + postalCode + ", name=" + name + "]";
	}
	
}
