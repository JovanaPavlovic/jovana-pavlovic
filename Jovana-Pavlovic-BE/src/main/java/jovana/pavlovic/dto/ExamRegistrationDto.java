package jovana.pavlovic.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

public class ExamRegistrationDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotNull(message = "Student can't be null.")
	private StudentDto student;
	
	@NotNull(message = "Exam can't be null.")
	private ExamDto exam;
	
	@NotNull(message = "Registration date can't be null.")
	private LocalDate registrationDate;
	
	public ExamRegistrationDto() {
		
	}

	public ExamRegistrationDto(Long id, StudentDto student, ExamDto exam, LocalDate registrationDate) {
		super();
		this.id = id;
		this.student = student;
		this.exam = exam;
		this.registrationDate = registrationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StudentDto getStudent() {
		return student;
	}

	public void setStudent(StudentDto student) {
		this.student = student;
	}

	public ExamDto getExam() {
		return exam;
	}

	public void setExam(ExamDto exam) {
		this.exam = exam;
	}

	public LocalDate getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDate registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public String toString() {
		return "ExamRegistrationDto [id=" + id + ", student=" + student + ", exam=" + exam + ", registrationDate="
				+ registrationDate + "]";
	}
	
}
