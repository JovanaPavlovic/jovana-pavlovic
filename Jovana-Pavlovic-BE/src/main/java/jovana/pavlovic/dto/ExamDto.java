package jovana.pavlovic.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotNull;

public class ExamDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotNull(message = "Subject can't be null.")
	private SubjectDto subject;
	
	@NotNull(message = "Professor can't be null.")
	private ProfessorDto professor;
	
	@NotNull(message = "Exam date can't be null.")
	private LocalDate examDate;
	
	public ExamDto() {
		
	}

	public ExamDto(Long id, SubjectDto subject, ProfessorDto professor, LocalDate examDate) {
		super();
		this.id = id;
		this.subject = subject;
		this.professor = professor;
		this.examDate = examDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectDto getSubject() {
		return subject;
	}

	public void setSubject(SubjectDto subject) {
		this.subject = subject;
	}

	public ProfessorDto getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorDto professor) {
		this.professor = professor;
	}

	public LocalDate getExamDate() {
		return examDate;
	}

	public void setExamDate(LocalDate examDate) {
		this.examDate = examDate;
	}

	@Override
	public String toString() {
		return "ExamDto [id=" + id + ", subject=" + subject + ", professor=" + professor + ", examDate=" + examDate
				+ "]";
	}

}
