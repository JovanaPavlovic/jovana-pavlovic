package jovana.pavlovic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.ExamRegistrationEntity;

@Repository
public interface ExamRegistrationRepository extends PagingAndSortingRepository<ExamRegistrationEntity, Long> {

}
