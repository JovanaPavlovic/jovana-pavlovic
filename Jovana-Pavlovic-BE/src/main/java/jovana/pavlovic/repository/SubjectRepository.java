package jovana.pavlovic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.SubjectEntity;

@Repository
public interface SubjectRepository extends PagingAndSortingRepository<SubjectEntity, Long> {

}
