package jovana.pavlovic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.ExamPeriodEntity;

@Repository
public interface ExamPeriodRepository extends PagingAndSortingRepository<ExamPeriodEntity, Long> {
}
