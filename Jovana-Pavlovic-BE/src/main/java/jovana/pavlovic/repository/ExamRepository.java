package jovana.pavlovic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.ExamEntity;

@Repository
public interface ExamRepository extends PagingAndSortingRepository<ExamEntity, Long> {

}
