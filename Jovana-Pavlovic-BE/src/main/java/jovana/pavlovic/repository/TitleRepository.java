package jovana.pavlovic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.TitleEntity;

@Repository
public interface TitleRepository extends PagingAndSortingRepository<TitleEntity, Long> {

}
