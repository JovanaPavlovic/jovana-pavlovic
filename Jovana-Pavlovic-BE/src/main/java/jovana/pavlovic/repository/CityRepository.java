package jovana.pavlovic.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.CityEntity;


@Repository
public interface CityRepository extends PagingAndSortingRepository<CityEntity, Long> {
	Optional<CityEntity> findByPostalCode(Long postalCode);

}
