package jovana.pavlovic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jovana.pavlovic.enity.ProfessorEntity;

@Repository
public interface ProfessorRepository extends PagingAndSortingRepository<ProfessorEntity, Long> {

}
