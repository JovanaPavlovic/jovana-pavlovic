package jovana.pavlovic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jovana.pavlovic.dto.StudentDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.service.StudentService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@GetMapping
	public @ResponseBody ResponseEntity<List<StudentDto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(studentService.findAll());
	}
	
	@GetMapping("/find/{id}")
	public @ResponseBody ResponseEntity<Object> findById(@PathVariable Long id) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(studentService.findById(id));
		} catch (NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}
	
	@GetMapping("/page")
	public @ResponseBody ResponseEntity<Page<StudentDto>> findByPage(Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(studentService.findByPage(pageable));
	}
	
	@PostMapping("/save")
	public @ResponseBody ResponseEntity<Object> save(@Valid @RequestBody StudentDto studentDto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(studentService.save(studentDto));
		} catch (ExistEntityException | NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public @ResponseBody ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			studentService.deleteById(id);
			return ResponseEntity.status(HttpStatus.OK).body("Student is deleted");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Student doesn't exist");
		}
	}
	
	@PostMapping("/update")
	public @ResponseBody ResponseEntity<Object> update(@Valid @RequestBody StudentDto studentDto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(studentService.update(studentDto));
		} catch (NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	} 

}
