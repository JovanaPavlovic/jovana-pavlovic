package jovana.pavlovic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jovana.pavlovic.dto.ExamRegistrationDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.InvalidDataException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.service.ExamRegistrationService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("examRegistration")
public class ExamRegistrationController {
	
	@Autowired
	private ExamRegistrationService examRegistrationService;
	
	@GetMapping
	public @ResponseBody ResponseEntity<List<ExamRegistrationDto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(examRegistrationService.findAll());
	}
	
	@GetMapping("/find/{id}")
	public @ResponseBody ResponseEntity<Object> findById(@PathVariable Long id) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(examRegistrationService.findById(id));
		} catch (NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}
	
	@GetMapping("/page")
	public @ResponseBody ResponseEntity<Page<ExamRegistrationDto>> findByPage(Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(examRegistrationService.findByPage(pageable));
	}
	
	@PostMapping("/save")
	public @ResponseBody ResponseEntity<Object> save(@Valid @RequestBody ExamRegistrationDto examRegistrationDto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(examRegistrationService.save(examRegistrationDto));
		} catch (ExistEntityException | NotExistEntityException | InvalidDataException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public @ResponseBody ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			examRegistrationService.deleteById(id);
			return ResponseEntity.status(HttpStatus.OK).body("Exam Registration is deleted");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Exam Registration doesn't exist");
		}
	}
	
	@PostMapping("/update")
	public @ResponseBody ResponseEntity<Object> update(@Valid @RequestBody ExamRegistrationDto examRegistrationDto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(examRegistrationService.update(examRegistrationDto));
		} catch (NotExistEntityException | InvalidDataException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	} 

}
