package jovana.pavlovic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jovana.pavlovic.dto.TitleDto;
import jovana.pavlovic.service.TitleService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("title")
public class TitleController {
	
	@Autowired
	private TitleService titleService;
	
	@GetMapping
	public @ResponseBody ResponseEntity<List<TitleDto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(titleService.findAll());
	}
	
}
