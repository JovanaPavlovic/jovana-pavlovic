package jovana.pavlovic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jovana.pavlovic.dto.SubjectDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.service.SubjectService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("subject")
public class SubjectController {
	
	@Autowired
	private SubjectService subjectService;
	
	@GetMapping
	public @ResponseBody ResponseEntity<List<SubjectDto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(subjectService.findAll());
	}
	
	@GetMapping("/find/{id}")
	public @ResponseBody ResponseEntity<Object> findById(@PathVariable Long id) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(subjectService.findById(id));
		} catch (NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}
	
	@GetMapping("/page")
	public @ResponseBody ResponseEntity<Page<SubjectDto>> findByPage(Pageable pageable) {
		return ResponseEntity.status(HttpStatus.OK).body(subjectService.findByPage(pageable));
	}
	
	@PostMapping("/save")
	public @ResponseBody ResponseEntity<Object> save(@Valid @RequestBody SubjectDto subjectDto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(subjectService.save(subjectDto));
		} catch (ExistEntityException | NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public @ResponseBody ResponseEntity<String> delete(@PathVariable Long id) {
		try {
			subjectService.deleteById(id);
			return ResponseEntity.status(HttpStatus.OK).body("Subject is deleted");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Subject doesn't exist");
		}
	}
	
	@PostMapping("/update")
	public @ResponseBody ResponseEntity<Object> update(@Valid @RequestBody SubjectDto subjectDto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(subjectService.update(subjectDto));
		} catch (NotExistEntityException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
	} 

}
