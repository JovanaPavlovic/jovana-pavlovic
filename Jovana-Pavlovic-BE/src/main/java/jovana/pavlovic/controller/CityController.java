package jovana.pavlovic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jovana.pavlovic.dto.CityDto;
import jovana.pavlovic.logging.Loggable;
import jovana.pavlovic.service.CityService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("city")
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@Loggable
	@GetMapping
	public @ResponseBody ResponseEntity<List<CityDto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(cityService.findAll());
	}
	
}
