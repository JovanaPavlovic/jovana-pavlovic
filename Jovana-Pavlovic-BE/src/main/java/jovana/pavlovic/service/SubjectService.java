package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jovana.pavlovic.dto.SubjectDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;

public interface SubjectService {
	List<SubjectDto> findAll();
	Optional<SubjectDto> findById(Long id) throws NotExistEntityException;
	Page<SubjectDto> findByPage(Pageable pageable);
	SubjectDto save(SubjectDto subjectDto) throws NotExistEntityException, ExistEntityException;
	void deleteById(Long id);
	SubjectDto update(SubjectDto subjectDto) throws NotExistEntityException;
}
