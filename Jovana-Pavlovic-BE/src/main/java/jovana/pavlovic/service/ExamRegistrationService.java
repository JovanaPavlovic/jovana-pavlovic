package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jovana.pavlovic.dto.ExamRegistrationDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.InvalidDataException;
import jovana.pavlovic.exception.NotExistEntityException;

public interface ExamRegistrationService {
	List<ExamRegistrationDto> findAll();
	Optional<ExamRegistrationDto> findById(Long id) throws NotExistEntityException;
	Page<ExamRegistrationDto> findByPage(Pageable pageable);
	ExamRegistrationDto save(ExamRegistrationDto examRegistrationDto) throws NotExistEntityException, ExistEntityException, InvalidDataException;
	void deleteById(Long id);
	ExamRegistrationDto update(ExamRegistrationDto examRegistrationDto) throws NotExistEntityException, InvalidDataException;

}
