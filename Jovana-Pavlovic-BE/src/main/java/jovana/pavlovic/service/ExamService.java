package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jovana.pavlovic.dto.ExamDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.InvalidDataException;
import jovana.pavlovic.exception.NotExistEntityException;

public interface ExamService {
	List<ExamDto> findAll();
	Optional<ExamDto> findById(Long id) throws NotExistEntityException;
	Page<ExamDto> findByPage(Pageable pageable);
	ExamDto save(ExamDto examDto) throws NotExistEntityException, ExistEntityException, InvalidDataException;
	void deleteById(Long id);
	ExamDto update(ExamDto examDto) throws NotExistEntityException, InvalidDataException;
}
