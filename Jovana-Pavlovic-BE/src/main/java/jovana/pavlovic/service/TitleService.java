package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import jovana.pavlovic.dto.TitleDto;

public interface TitleService {
	List<TitleDto> findAll();
	Optional<TitleDto> findById(Long id);

}
