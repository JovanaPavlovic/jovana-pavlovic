package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jovana.pavlovic.dto.ProfessorDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;

public interface ProfessorService {
	List<ProfessorDto> findAll();
	Optional<ProfessorDto> findById(Long id) throws NotExistEntityException;
	Page<ProfessorDto> findByPage(Pageable pageable);
	ProfessorDto save(ProfessorDto professorDto) throws NotExistEntityException, ExistEntityException;
	void deleteById(Long id);
	ProfessorDto update(ProfessorDto professorDto) throws NotExistEntityException;

}
