package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import jovana.pavlovic.dto.CityDto;

public interface CityService {
	List<CityDto> findAll();
	Optional<CityDto> findByPostalCode(Long postalCode);

}
