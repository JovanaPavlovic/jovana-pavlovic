package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jovana.pavlovic.dto.StudentDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;

public interface StudentService {
	List<StudentDto> findAll();
	Optional<StudentDto> findById(Long id) throws NotExistEntityException;
	Page<StudentDto> findByPage(Pageable pageable);
	StudentDto save(StudentDto studentDto) throws NotExistEntityException, ExistEntityException;
	void deleteById(Long id);
	StudentDto update(StudentDto studentDto) throws NotExistEntityException;
	
}