package jovana.pavlovic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jovana.pavlovic.dto.ExamPeriodDto;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;

public interface ExamPeriodService {
	List<ExamPeriodDto> findAll();
	Optional<ExamPeriodDto> findById(Long id) throws NotExistEntityException;
	Page<ExamPeriodDto> findByPage(Pageable pageable);
	ExamPeriodDto save(ExamPeriodDto examPeriodDto) throws NotExistEntityException, ExistEntityException;
	void deleteById(Long id);
	ExamPeriodDto update(ExamPeriodDto examPeriodDto) throws NotExistEntityException;

}
