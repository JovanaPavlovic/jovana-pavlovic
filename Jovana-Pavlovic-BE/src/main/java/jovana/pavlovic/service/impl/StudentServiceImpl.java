package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.StudentDto;
import jovana.pavlovic.enity.CityEntity;
import jovana.pavlovic.enity.StudentEntity;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.mapper.StudentMapper;
import jovana.pavlovic.repository.CityRepository;
import jovana.pavlovic.repository.StudentRepository;
import jovana.pavlovic.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	private final StudentRepository studentRepository;
	private final CityRepository cityRepository;
	private final StudentMapper studentMapper = Mappers.getMapper(StudentMapper.class);
	
	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository, CityRepository cityRepository) {
		this.studentRepository = studentRepository;
		this.cityRepository = cityRepository;
	}
	
	@Override
	public List<StudentDto> findAll() {
		List<StudentEntity> students = (List<StudentEntity>) studentRepository.findAll();
		return students.stream().map(student -> {
			return studentMapper.toStudentDto(student);
		}).collect(Collectors.toList());
	}
	
	@Override
	public Optional<StudentDto> findById(Long id) throws NotExistEntityException {
		Optional<StudentEntity> student = studentRepository.findById(id);
		if(student.isEmpty()) {
			throw new NotExistEntityException(id, "Student with id " + id + " don't exists");
		}
		return Optional.of(studentMapper.toStudentDto(student.get()));
	}
	
	@Override
	public Page<StudentDto> findByPage(Pageable pageable) {
		Page<StudentDto> students = studentRepository.findAll(pageable).map(studentMapper::toStudentDto);
		return students;
	}

	@Override
	public StudentDto save(StudentDto studentDto) throws NotExistEntityException, ExistEntityException {
		if(studentDto.getId() != null) {
			Optional<StudentEntity> existingStudent = studentRepository.findById(studentDto.getId());
			if(existingStudent.isPresent()) {
				throw new ExistEntityException(existingStudent, "Student already exists");
			}
		}
		
		Optional<CityEntity> existingCity = cityRepository.findByPostalCode(studentDto.getCity().getPostalCode());
		if(existingCity.isEmpty()) {
			throw new NotExistEntityException(studentDto, "City don't exists");
		}
		
		StudentEntity student = studentMapper.toStudentEntity(studentDto);
		studentRepository.save(student);
		return studentMapper.toStudentDto(student);
	}

	@Override
	public void deleteById(Long id) {
		studentRepository.deleteById(id);
	}

	@Override
	public StudentDto update(StudentDto studentDto) throws NotExistEntityException {
		Optional<StudentEntity> existingStudent = studentRepository.findById(studentDto.getId());
		if(existingStudent.isEmpty()) {
			throw new NotExistEntityException(studentDto, "Student don't exists");
		}
		
		Optional<CityEntity> existingCity = cityRepository.findByPostalCode(studentDto.getCity().getPostalCode());
		if(existingCity.isEmpty()) {
			throw new NotExistEntityException(studentDto, "City don't exists");
		}

		StudentEntity student = studentMapper.toStudentEntity(studentDto);
		student = studentRepository.save(student);
		return studentMapper.toStudentDto(student);

	}

}
