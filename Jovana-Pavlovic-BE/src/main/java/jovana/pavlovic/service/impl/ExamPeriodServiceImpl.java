package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.ExamPeriodDto;
import jovana.pavlovic.enity.ExamPeriodEntity;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.mapper.ExamPeriodMapper;
import jovana.pavlovic.repository.ExamPeriodRepository;
import jovana.pavlovic.service.ExamPeriodService;

@Service
@Transactional
public class ExamPeriodServiceImpl implements ExamPeriodService {
	private final ExamPeriodRepository examPeriodRepository;
	private final ExamPeriodMapper examPeriodMapper = Mappers.getMapper(ExamPeriodMapper.class);
	
	public ExamPeriodServiceImpl(ExamPeriodRepository examPeriodRepository) {
		this.examPeriodRepository = examPeriodRepository;
	}

	@Override
	public List<ExamPeriodDto> findAll() {
		List<ExamPeriodEntity> examPeriods = (List<ExamPeriodEntity>) examPeriodRepository.findAll();
		return examPeriods.stream().map(examPeriod -> {
			return examPeriodMapper.toExamPeriodDto(examPeriod);
		}).collect(Collectors.toList());
	}

	@Override
	public Optional<ExamPeriodDto> findById(Long id) throws NotExistEntityException {
		Optional<ExamPeriodEntity> examPeriod = examPeriodRepository.findById(id);
		if(examPeriod.isEmpty()) {
			throw new NotExistEntityException(id, "Exam Period with id " + id + " don't exists");
		}
		return Optional.of(examPeriodMapper.toExamPeriodDto(examPeriod.get()));
	}

	@Override
	public Page<ExamPeriodDto> findByPage(Pageable pageable) {
		Page<ExamPeriodDto> examPeriods = examPeriodRepository.findAll(pageable).map(examPeriodMapper::toExamPeriodDto);
		return examPeriods;
	}

	@Override
	public ExamPeriodDto save(ExamPeriodDto examPeriodDto) throws NotExistEntityException, ExistEntityException {
		if(examPeriodDto.getId() != null) {
			Optional<ExamPeriodEntity> existingExamPeriod = examPeriodRepository.findById(examPeriodDto.getId());
			if(existingExamPeriod.isPresent()) {
				throw new ExistEntityException(existingExamPeriod, "Exam Period already exists");
			}
		}
		
		ExamPeriodEntity examPeriod = examPeriodMapper.toExamPeriodEntity(examPeriodDto);
		examPeriodRepository.save(examPeriod);
		return examPeriodMapper.toExamPeriodDto(examPeriod);
	}

	@Override
	public void deleteById(Long id) {
		examPeriodRepository.deleteById(id);
		
	}

	@Override
	public ExamPeriodDto update(ExamPeriodDto examPeriodDto) throws NotExistEntityException {
		Optional<ExamPeriodEntity> existingExamPeriod = examPeriodRepository.findById(examPeriodDto.getId());
		if(existingExamPeriod.isEmpty()) {
			throw new NotExistEntityException(examPeriodDto, "Exam Period don't exists");
		}
		
		ExamPeriodEntity examPeriod = examPeriodMapper.toExamPeriodEntity(examPeriodDto);
		examPeriod = examPeriodRepository.save(examPeriod);
		return examPeriodMapper.toExamPeriodDto(examPeriod);
	}

}
