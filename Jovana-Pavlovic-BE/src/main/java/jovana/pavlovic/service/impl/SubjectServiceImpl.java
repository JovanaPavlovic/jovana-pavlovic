package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.SubjectDto;
import jovana.pavlovic.enity.SubjectEntity;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.mapper.SubjectMapper;
import jovana.pavlovic.repository.SubjectRepository;
import jovana.pavlovic.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {
	private final SubjectRepository subjectRepository;
	private final SubjectMapper subjectMapper = Mappers.getMapper(SubjectMapper.class);
	
	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository) {
		this.subjectRepository = subjectRepository;
	}

	@Override
	public List<SubjectDto> findAll() {
		List<SubjectEntity> subjects = (List<SubjectEntity>) subjectRepository.findAll();
		return subjects.stream().map(subject -> {
			return subjectMapper.toSubjectDto(subject);
		}).collect(Collectors.toList());
	}

	@Override
	public Optional<SubjectDto> findById(Long id) throws NotExistEntityException {
		Optional<SubjectEntity> subject = subjectRepository.findById(id);
		if(subject.isEmpty()) {
			throw new NotExistEntityException(id, "Subject with id " + id + " don't exists");
		}
		return Optional.of(subjectMapper.toSubjectDto(subject.get()));
	}

	@Override
	public Page<SubjectDto> findByPage(Pageable pageable) {
		Page<SubjectDto> subjects = subjectRepository.findAll(pageable).map(subjectMapper::toSubjectDto);
		return subjects;
	}

	@Override
	public SubjectDto save(SubjectDto subjectDto) throws NotExistEntityException, ExistEntityException {
		if(subjectDto.getId() != null) {
			Optional<SubjectEntity> existingSubject = subjectRepository.findById(subjectDto.getId());
			if(existingSubject.isPresent()) {
				throw new ExistEntityException(existingSubject, "Subject already exists");
			}
		}
		
		SubjectEntity subject = subjectMapper.toSubjectEntity(subjectDto);
		subjectRepository.save(subject);
		return subjectMapper.toSubjectDto(subject);
	}

	@Override
	public void deleteById(Long id) {
		subjectRepository.deleteById(id);
		
	}

	@Override
	public SubjectDto update(SubjectDto subjectDto) throws NotExistEntityException {
		Optional<SubjectEntity> existingSubject = subjectRepository.findById(subjectDto.getId());
		if(existingSubject.isEmpty()) {
			throw new NotExistEntityException(subjectDto, "Subject don't exists");
		}
		
		SubjectEntity subject = subjectMapper.toSubjectEntity(subjectDto);
		subject = subjectRepository.save(subject);
		return subjectMapper.toSubjectDto(subject);
	}

}
