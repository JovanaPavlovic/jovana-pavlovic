package jovana.pavlovic.service.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.ExamRegistrationDto;
import jovana.pavlovic.enity.ExamEntity;
import jovana.pavlovic.enity.ExamRegistrationEntity;
import jovana.pavlovic.enity.StudentEntity;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.InvalidDataException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.mapper.ExamRegistrationMapper;
import jovana.pavlovic.repository.ExamRegistrationRepository;
import jovana.pavlovic.repository.ExamRepository;
import jovana.pavlovic.repository.StudentRepository;
import jovana.pavlovic.service.ExamRegistrationService;

@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService{
	private final ExamRegistrationRepository examRegistrationRepository;
	private final StudentRepository studentRepository;
	private final ExamRepository examRepository;
	private final ExamRegistrationMapper examRegistrationMapper = Mappers.getMapper(ExamRegistrationMapper.class);
	
	public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository, StudentRepository studentRepository, 
			ExamRepository examRepository) {
		this.examRegistrationRepository = examRegistrationRepository;
		this.studentRepository = studentRepository;
		this.examRepository = examRepository;
	}

	@Override
	public List<ExamRegistrationDto> findAll() {
		List<ExamRegistrationEntity> examRegistrations = (List<ExamRegistrationEntity>) examRegistrationRepository.findAll();
		return examRegistrations.stream().map(examRegistration -> {
			return examRegistrationMapper.toExamRegistrationDto(examRegistration);
		}).collect(Collectors.toList());
	}

	@Override
	public Optional<ExamRegistrationDto> findById(Long id) throws NotExistEntityException {
		Optional<ExamRegistrationEntity> examRegistration = examRegistrationRepository.findById(id);
		if(examRegistration.isEmpty()) {
			throw new NotExistEntityException(id, "Exam Registration with id " + id + " don't exists");
		}
		return Optional.of(examRegistrationMapper.toExamRegistrationDto(examRegistration.get()));
	}

	@Override
	public Page<ExamRegistrationDto> findByPage(Pageable pageable) {
		Page<ExamRegistrationDto> examRegistrations = examRegistrationRepository.findAll(pageable).map(examRegistrationMapper::toExamRegistrationDto);
		return examRegistrations;
	}

	@Override
	public ExamRegistrationDto save(ExamRegistrationDto examRegistrationDto) throws NotExistEntityException, ExistEntityException, InvalidDataException {
		if(examRegistrationDto.getId() != null) {
			Optional<ExamRegistrationEntity> existingExamRegistration = examRegistrationRepository.findById(examRegistrationDto.getId());
			if(existingExamRegistration.isPresent()) {
				throw new ExistEntityException(existingExamRegistration, "Exam Registration already exists");
			}
		}
		
		Optional<StudentEntity> existingStudent = studentRepository.findById(examRegistrationDto.getStudent().getId());
		if(existingStudent.isEmpty()) {
			throw new NotExistEntityException(examRegistrationDto, "Student don't exists");
		}
		
		Optional<ExamEntity> existingExam = examRepository.findById(examRegistrationDto.getExam().getId());
		if(existingExam.isEmpty()) {
			throw new NotExistEntityException(examRegistrationDto, "Exam don't exists");
		}
		
		if(examRegistrationDto.getStudent().getCurrentYearOfStudy() < examRegistrationDto.getExam().getSubject().getYearOfStudy()) {
			throw new InvalidDataException(examRegistrationDto, "The subject must be attended before it can be registrated.");
		} 
		
		if(!appropriateDate(examRegistrationDto.getRegistrationDate(), examRegistrationDto.getExam().getExamDate())) {
			throw new InvalidDataException(examRegistrationDto, "The subject can be registrated only 7 days before exam.");
		}
		
		ExamRegistrationEntity examRegistration = examRegistrationMapper.toExamRegistrationEntity(examRegistrationDto);
		examRegistrationRepository.save(examRegistration);
		return examRegistrationMapper.toExamRegistrationDto(examRegistration);
	}
	
	private boolean appropriateDate(LocalDate registrationDate, LocalDate examDate) {
		if(ChronoUnit.DAYS.between(registrationDate, examDate) > 0 && ChronoUnit.DAYS.between(registrationDate, examDate) <= 7) {
			return true;
		}
		return false;
	}

	@Override
	public void deleteById(Long id) {
		examRegistrationRepository.deleteById(id);
		
	}

	@Override
	public ExamRegistrationDto update(ExamRegistrationDto examRegistrationDto) throws NotExistEntityException, InvalidDataException {
		Optional<ExamRegistrationEntity> existingExamRegistration = examRegistrationRepository.findById(examRegistrationDto.getId());
		if(existingExamRegistration.isEmpty()) {
			throw new NotExistEntityException(examRegistrationDto, "Exam Registration don't exists");
		}
		
		Optional<StudentEntity> existingStudent = studentRepository.findById(examRegistrationDto.getStudent().getId());
		if(existingStudent.isEmpty()) {
			throw new NotExistEntityException(examRegistrationDto, "Student don't exists");
		}
		
		Optional<ExamEntity> existingExam = examRepository.findById(examRegistrationDto.getExam().getId());
		if(existingExam.isEmpty()) {
			throw new NotExistEntityException(examRegistrationDto, "Exam don't exists");
		}
		
		if(examRegistrationDto.getStudent().getCurrentYearOfStudy() < examRegistrationDto.getExam().getSubject().getYearOfStudy()) {
			throw new InvalidDataException(examRegistrationDto, "The subject must be attended before it can be registrated.");
		} 
		
		if(!appropriateDate(examRegistrationDto.getRegistrationDate(), examRegistrationDto.getExam().getExamDate())) {
			throw new InvalidDataException(examRegistrationDto, "The subject can be registrated only 7 days before exam.");
		}
		
		ExamRegistrationEntity examRegistration = examRegistrationMapper.toExamRegistrationEntity(examRegistrationDto);
		examRegistration = examRegistrationRepository.save(examRegistration);
		return examRegistrationMapper.toExamRegistrationDto(examRegistration);
	}

}
