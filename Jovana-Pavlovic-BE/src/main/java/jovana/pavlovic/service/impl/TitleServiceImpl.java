package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.TitleDto;
import jovana.pavlovic.enity.TitleEntity;
import jovana.pavlovic.mapper.TitleMapper;
import jovana.pavlovic.repository.TitleRepository;
import jovana.pavlovic.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {
	private final TitleRepository titleRepository;
	private final TitleMapper titleMapper = Mappers.getMapper(TitleMapper.class);
	
	
	public TitleServiceImpl(TitleRepository titleRepository) {
		this.titleRepository = titleRepository;
	}
	
	@Override
	public List<TitleDto> findAll() {
		List<TitleEntity> titles = (List<TitleEntity>) titleRepository.findAll();
		return titles.stream().map(title -> {
			return titleMapper.toTitleDto(title);
		}).collect(Collectors.toList());
	} 

	@Override
	public Optional<TitleDto> findById(Long id) {
		Optional<TitleEntity> title = titleRepository.findById(id);
		if(title.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(titleMapper.toTitleDto(title.get()));
	}

}
