package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.CityDto;
import jovana.pavlovic.enity.CityEntity;
import jovana.pavlovic.mapper.CityMapper;
import jovana.pavlovic.repository.CityRepository;
import jovana.pavlovic.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	private final CityRepository cityRepository;
	private final CityMapper cityMapper = Mappers.getMapper(CityMapper.class);
	
	@Autowired
	public CityServiceImpl(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}

	@Override
	public List<CityDto> findAll() {
		List<CityEntity> cities = (List<CityEntity>) cityRepository.findAll();
		return cities.stream().map(city -> {
			return cityMapper.toCityDto(city);
		}).collect(Collectors.toList());
	} 

	@Override
	public Optional<CityDto> findByPostalCode(Long postalCode) {
		Optional<CityEntity> city = cityRepository.findByPostalCode(postalCode);
		if(city.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(cityMapper.toCityDto(city.get()));
	}
	
}
