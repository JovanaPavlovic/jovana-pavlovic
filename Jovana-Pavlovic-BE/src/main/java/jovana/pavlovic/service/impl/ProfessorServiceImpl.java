package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.ProfessorDto;
import jovana.pavlovic.enity.CityEntity;
import jovana.pavlovic.enity.ProfessorEntity;
import jovana.pavlovic.enity.TitleEntity;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.mapper.ProfessorMapper;
import jovana.pavlovic.repository.CityRepository;
import jovana.pavlovic.repository.ProfessorRepository;
import jovana.pavlovic.repository.TitleRepository;
import jovana.pavlovic.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {
	private final ProfessorRepository professorRepository;
	private final CityRepository cityRepository;
	private final TitleRepository titleRepository;
	private final ProfessorMapper professorMapper = Mappers.getMapper(ProfessorMapper.class);
	
	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository, CityRepository cityRepository, TitleRepository titleRepository) {
		this.professorRepository = professorRepository;
		this.cityRepository = cityRepository;
		this.titleRepository = titleRepository;
	}

	@Override
	public List<ProfessorDto> findAll() {
		List<ProfessorEntity> professors = (List<ProfessorEntity>) professorRepository.findAll();
		return professors.stream().map(professor -> {
			return professorMapper.toProfessorDto(professor);
		}).collect(Collectors.toList());
	}

	@Override
	public Optional<ProfessorDto> findById(Long id) throws NotExistEntityException {
		Optional<ProfessorEntity> professor = professorRepository.findById(id);
		if(professor.isEmpty()) {
			throw new NotExistEntityException(id, "Professor with id " + id + " don't exists");
		}
		return Optional.of(professorMapper.toProfessorDto(professor.get()));
	}

	@Override
	public Page<ProfessorDto> findByPage(Pageable pageable) {
		Page<ProfessorDto> professors = professorRepository.findAll(pageable).map(professorMapper::toProfessorDto);
		return professors;
	}

	@Override
	public ProfessorDto save(ProfessorDto professorDto) throws NotExistEntityException, ExistEntityException {
		if(professorDto.getId() != null) {
			Optional<ProfessorEntity> existingProfessor = professorRepository.findById(professorDto.getId());
			if(existingProfessor.isPresent()) {
				throw new ExistEntityException(existingProfessor, "Professor already exists");
			}
		}
		
		Optional<CityEntity> existingCity = cityRepository.findByPostalCode(professorDto.getCity().getPostalCode());
		if(existingCity.isEmpty()) {
			throw new NotExistEntityException(professorDto, "City don't exists");
		}
		
		Optional<TitleEntity> existingTitle = titleRepository.findById(professorDto.getTitle().getId());
		if(existingTitle.isEmpty()) {
			throw new NotExistEntityException(professorDto, "Title don't exists");
		}
		
		ProfessorEntity professor = professorMapper.toProfessorEntity(professorDto);
		professorRepository.save(professor);
		return professorMapper.toProfessorDto(professor);
	}

	@Override
	public void deleteById(Long id) {
		professorRepository.deleteById(id);
		
	}

	@Override
	public ProfessorDto update(ProfessorDto professorDto) throws NotExistEntityException {
		Optional<ProfessorEntity> existingProfessor = professorRepository.findById(professorDto.getId());
		if(existingProfessor.isEmpty()) {
			throw new NotExistEntityException(professorDto, "Professor don't exists");
		}
		
		Optional<CityEntity> existingCity = cityRepository.findByPostalCode(professorDto.getCity().getPostalCode());
		if(existingCity.isEmpty()) {
			throw new NotExistEntityException(professorDto, "City don't exists");
		}
		
		Optional<TitleEntity> existingTitle = titleRepository.findById(professorDto.getTitle().getId());
		if(existingTitle.isEmpty()) {
			throw new NotExistEntityException(professorDto, "Title don't exists");
		}

		ProfessorEntity professor = professorMapper.toProfessorEntity(professorDto);
		professor = professorRepository.save(professor);
		return professorMapper.toProfessorDto(professor);
	}

}
