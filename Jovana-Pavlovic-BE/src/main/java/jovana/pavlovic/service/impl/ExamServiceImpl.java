package jovana.pavlovic.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jovana.pavlovic.dto.ExamDto;
import jovana.pavlovic.dto.ExamPeriodDto;
import jovana.pavlovic.enity.ExamEntity;
import jovana.pavlovic.enity.ExamPeriodEntity;
import jovana.pavlovic.enity.ProfessorEntity;
import jovana.pavlovic.enity.SubjectEntity;
import jovana.pavlovic.exception.ExistEntityException;
import jovana.pavlovic.exception.InvalidDataException;
import jovana.pavlovic.exception.NotExistEntityException;
import jovana.pavlovic.mapper.ExamMapper;
import jovana.pavlovic.mapper.ExamPeriodMapper;
import jovana.pavlovic.repository.ExamPeriodRepository;
import jovana.pavlovic.repository.ExamRepository;
import jovana.pavlovic.repository.ProfessorRepository;
import jovana.pavlovic.repository.SubjectRepository;
import jovana.pavlovic.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService {
	private final ExamRepository examRepository;
	private final SubjectRepository subjectRepository;
	private final ProfessorRepository professorRepository;
	private final ExamPeriodRepository examPeriodRepository;
	private final ExamMapper examMapper = Mappers.getMapper(ExamMapper.class);
	private final ExamPeriodMapper examPeriodMapper = Mappers.getMapper(ExamPeriodMapper.class);
	
	@Autowired
	public ExamServiceImpl(ExamRepository examRepository, SubjectRepository subjectRepository, ProfessorRepository professorRepository, ExamPeriodRepository examPeriodRepository) {
		this.examRepository = examRepository;
		this.subjectRepository = subjectRepository;
		this.professorRepository = professorRepository;
		this.examPeriodRepository = examPeriodRepository;
	}

	@Override
	public List<ExamDto> findAll() {
		List<ExamEntity> exams = (List<ExamEntity>) examRepository.findAll();
		return exams.stream().map(exam -> {
			return examMapper.toExamDto(exam);
		}).collect(Collectors.toList());
	}

	@Override
	public Optional<ExamDto> findById(Long id) throws NotExistEntityException {
		Optional<ExamEntity> exam = examRepository.findById(id);
		if(exam.isEmpty()) {
			throw new NotExistEntityException(id, "Exam with id " + id + " don't exists");
		}
		return Optional.of(examMapper.toExamDto(exam.get()));
	}

	@Override
	public Page<ExamDto> findByPage(Pageable pageable) {
		Page<ExamDto> exams = examRepository.findAll(pageable).map(examMapper::toExamDto);
		return exams;
	}

	@Override
	public ExamDto save(ExamDto examDto) throws NotExistEntityException, ExistEntityException, InvalidDataException {
		if(examDto.getId() != null) {
			Optional<ExamEntity> existingExam = examRepository.findById(examDto.getId());
			if(existingExam.isPresent()) {
				throw new ExistEntityException(existingExam, "Exam already exists");
			}
		}
		
		Optional<SubjectEntity> existingSubject = subjectRepository.findById(examDto.getSubject().getId());
		if(existingSubject.isEmpty()) {
			throw new NotExistEntityException(examDto, "Subject don't exists");
		}
		
		Optional<ProfessorEntity> existingProfessor = professorRepository.findById(examDto.getProfessor().getId());
		if(existingProfessor.isEmpty()) {
			throw new NotExistEntityException(examDto, "Professor don't exists");
		}
		
		List<ExamPeriodEntity> examPeriodEntities = (List<ExamPeriodEntity>) examPeriodRepository.findAll();
		List<ExamPeriodDto> examPeriods = examPeriodEntities.stream().map(examPeriodEntity -> {
			return examPeriodMapper.toExamPeriodDto(examPeriodEntity);
		}).collect(Collectors.toList());
		
		for(ExamPeriodDto examPeriod : examPeriods) {
			if(examPeriod.getExams().contains(examDto) && examDto.getExamDate().isAfter(examPeriod.getStartOfExamPeriod()) 
					&& examDto.getExamDate().isBefore(examPeriod.getEndOfExamPeriod())) {
				throw new InvalidDataException(examDto, "You can't define two same exams in one exam period.");
			}  
		}
		
		ExamEntity exam = examMapper.toExamEntity(examDto);
		examRepository.save(exam);
		return examMapper.toExamDto(exam);
	}

	@Override
	public void deleteById(Long id) {
		examRepository.deleteById(id);
		
	}

	@Override
	public ExamDto update(ExamDto examDto) throws NotExistEntityException, InvalidDataException {
		Optional<ExamEntity> existingExam = examRepository.findById(examDto.getId());
		if(existingExam.isEmpty()) {
			throw new NotExistEntityException(examDto, "Exam don't exists");
		}
		
		Optional<SubjectEntity> existingSubject = subjectRepository.findById(examDto.getSubject().getId());
		if(existingSubject.isEmpty()) {
			throw new NotExistEntityException(examDto, "Subject don't exists");
		}
		
		Optional<ProfessorEntity> existingProfessor = professorRepository.findById(examDto.getProfessor().getId());
		if(existingProfessor.isEmpty()) {
			throw new NotExistEntityException(examDto, "Professor don't exists");
		}
		
		List<ExamPeriodEntity> examPeriodEntities = (List<ExamPeriodEntity>) examPeriodRepository.findAll();
		List<ExamPeriodDto> examPeriods = examPeriodEntities.stream().map(examPeriodEntity -> {
			return examPeriodMapper.toExamPeriodDto(examPeriodEntity);
		}).collect(Collectors.toList());
		
		for(ExamPeriodDto examPeriod : examPeriods) {
			if(examPeriod.getExams().contains(examDto) && examDto.getExamDate().isAfter(examPeriod.getStartOfExamPeriod()) 
					&& examDto.getExamDate().isBefore(examPeriod.getEndOfExamPeriod())) {
				throw new InvalidDataException(examDto, "You can't define two same exams in one exam period.");
			}  
		}
		

		ExamEntity exam = examMapper.toExamEntity(examDto);
		examRepository.save(exam);
		return examMapper.toExamDto(exam);
	}

}
