package jovana.pavlovic.exception;

public class MyApplicationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	MyApplicationException(String message) {
		super(message);
	}

}
