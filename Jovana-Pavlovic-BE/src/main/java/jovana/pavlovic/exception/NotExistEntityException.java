package jovana.pavlovic.exception;

public class NotExistEntityException extends MyApplicationException {
	private static final long serialVersionUID = 1L;
	
	private final Object entity;
	
	public NotExistEntityException(Object entity, String message) {
		super(message);
		this.entity = entity;
	}
	
	public Object getEntity() {
		return entity;
	}

}
