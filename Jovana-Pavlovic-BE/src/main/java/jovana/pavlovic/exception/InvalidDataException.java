package jovana.pavlovic.exception;

public class InvalidDataException extends MyApplicationException {
	private static final long serialVersionUID = 1L;
	
	private final Object entity;
	
	public InvalidDataException(Object entity, String message) {
		super(message);
		this.entity = entity;
	}
	
	public Object getEntity() {
		return entity;
	}


}
